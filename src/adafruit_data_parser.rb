#!/usr/bin/ruby
# adafruit_data_parser.rb

require_relative 'stats_array.rb'

$VOLTAGE_TYPE = "voltage"
$AMPERAGE_TYPE = "amperage"
$WATTAGE_TYPE = "wattage"

$OUTPUT_DIR = "output/"

# Ensure there is a filename argument.
if ARGV.length != 1
    raise ArgumentError, "\nUsage: ruby adafruit_data_parser.rb filename"
end

filename = ARGV[0]

voltage_array, amperage_array, wattage_array = [], [], []
voltage_hash, amperage_hash, wattage_hash = {}, {}, {}

# This regexp for test string:
# V: 5.2 I: 383 mA Watts: 2.0
# Resulting match groups:
# 1.  5.2
# 2.  .2
# 3.  383
# 4.   
# 5.  2.0
# 6.  .0
regexp = /V:\s*(\d*(.\d*)?)\sI:\s*(\d*(.\d*)?)mA\sWatts:\s*(\d*(.\d*)?)/

# This regexp for test string:
# des_Calamaria-iMac13,2-OSX10,8,5
# Resulting match groups:
# 1.  des_Calamaria (ID)
# 2.  s
# 3.  a
# 4.  iMac13,2 (Model)
# 5.  2
# 6.  OSX10,8,5 (OS)
# 7.  5
$MACHINE_ATTR_REGEXP = /((\w)+_(\w)+)-((\w|,)*)-((\w|,)*)/

# Convert input_filename to output_filename.
def output_filename(input_filename)
    return $OUTPUT_DIR + File.basename(input_filename)
end

# Accumulates value into buckets in hash.
def add_value_to_hash(hash, value)
    if hash.has_key? value
        hash[value] = hash[value] + 1
    else
        hash[value] = 1
    end
end

# Converts hash bucket values to percentages for CDF plots. 
# TODO: make this more efficient.
def convert_values_to_percentages(hash)
    values_sum = 0
    hash.each_value do |value|
        values_sum += value
    end

    hash.each do |key, value|
        hash[key] = value.to_f / values_sum.to_f
    end

    return hash

end

# Changes hash values to cumulative for CDF. E.g. if hash = { 1 => .1, 2 => .6, 3 => .3 }
# output would be hash = { 1 => .1, 2 => .7, 3 => 1.0 }.
def accumulate_percentages(hash)
    hash = Hash[hash.sort]
    accumulator = 0
    hash.each do |key,value|
        hash[key] = value + accumulator
        accumulator = hash[key]
    end

    return hash
end

def write_hash_CDF(input_filename, type, hash)

    f = File.open("#{output_filename(input_filename)}_#{type}_cdf", "w")
    hash.each_with_index do |entry, index|
        f.write "#{entry[0]} \t #{entry[1]}"
        f.write "\n" if index < (hash.length - 1)
    end
    f.close
end

# Writes samples for measurements over time graph.
def write_samples_for_measurements_over_time_helper input_filename, measurement_array, measurement_type
    # Need to copy the input array so it is not changed by Array#shift.
    copy = Array.new(measurement_array)
    measurement_counter = 1;
    # Writes 50 measurements for measurement over time graph.
    # Replace 5 with any number x to get x * 10 measurements in the output.
    for i in 1..5
        temp = copy.shift(10)
        f = File.open("#{output_filename(input_filename)}_#{measurement_type}_time.o", "a")
        temp.each_with_index do |value, index|
            f.write "#{measurement_counter} \t #{value}"
            f.write "\n"
            measurement_counter += 1
        end
        f.close
    end
end

# Writes CDF output to files for voltage, amperage, and wattage.
def write_samples_for_CDF filename, voltage_hash, amperage_hash, wattage_hash
    write_hash_CDF filename, $VOLTAGE_TYPE, accumulate_percentages(convert_values_to_percentages(voltage_hash))
    write_hash_CDF filename, $AMPERAGE_TYPE, accumulate_percentages(convert_values_to_percentages(amperage_hash))
    write_hash_CDF filename, $WATTAGE_TYPE, accumulate_percentages(convert_values_to_percentages(wattage_hash))
end

# Appends samples in CSV format to input_filename.csv.
def write_samples_csv input_filename, voltage_array, amperage_array, wattage_array
    match = input_filename.match $MACHINE_ATTR_REGEXP

    if match.nil?
        raise "Machine Attributes not parsed properly for #{input_filename}."
    end

    machine_id = match[1]
    machine_os = match[6].gsub(',', '.')
    machine_model = match[4].gsub(',', '.')

    f = File.open("#{output_filename(input_filename)}.csv", "a")
    
    if File.zero?(f)
        # The csv file has nothing in it so write the header row.
        f.write "Machine ID, Machine OS, Machine Model, Voltage Mean, Voltage Mode, Voltage Standard Deviation, Amperage Mean, Amperage Mode, Amperage Standard Deviation, Wattage Mean, Wattage Mode, Wattage Standard Deviation\n"
    end

    f.write "#{machine_id},#{machine_os},#{machine_model},#{voltage_array.mean},#{voltage_array.mode},#{voltage_array.stddev},#{amperage_array.mean},#{amperage_array.mode},#{amperage_array.stddev},#{wattage_array.mean},#{wattage_array.mode},#{wattage_array.stddev}\n"
    f.close
end

def write_samples_for_measurements_over_time filename, voltage_array, amperage_array, wattage_array
    write_samples_for_measurements_over_time_helper filename, voltage_array, $VOLTAGE_TYPE
    write_samples_for_measurements_over_time_helper filename, amperage_array, $AMPERAGE_TYPE
    write_samples_for_measurements_over_time_helper filename, wattage_array, $WATTAGE_TYPE
end

begin

    file = File.new(filename, "r")
    for i in 1..50
        for j in 1..10
            line = file.gets
            puts "line ${((i-1)*10 + j)} is null in #{filename}" if line.nil?
            match = line.match regexp
            if match.nil?
                # Unusable line; set counter back by 1.
                j -= 1
                next
            end
            v = match[1].to_f
            a = match[3].to_f
            w = match[5].to_f
            voltage_array << v
            amperage_array << a
            wattage_array << w
            add_value_to_hash(voltage_hash, v)
            add_value_to_hash(amperage_hash, a)
            add_value_to_hash(wattage_hash, w)
        end
    write_samples_csv filename, voltage_array, amperage_array, wattage_array
    end
    file.close

    write_samples_for_measurements_over_time filename, voltage_array, amperage_array, wattage_array
    write_samples_for_CDF filename, voltage_hash, amperage_hash, wattage_hash
    
rescue => err
    puts "Exception: #{err}"
    raise
end
