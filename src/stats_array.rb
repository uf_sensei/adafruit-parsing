# Extend Array class adding functions for mean, mode
# and standard deviation.
class Array
    def sum 
        inject { |sum,x| sum + x } 
    end

    def mean
        sum / size
    end

    def mode
        group_by do |e|
          e
        end.values.max_by(&:size).first
    end

    def variance
      m = mean
      m_sum = inject(0) { |accum, i| accum +(i-m)**2 }
      m_sum/length
    end

    def stddev
        return Math.sqrt(variance)
    end
end