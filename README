## Adafruit Power Data Processing

CLI command: src/adafruit_data_parser.rb data/raw/X

where 'X' is the data you would like to parse for CDF and Measurement over time graphs and/or csv output of features used in the project (e.g. `src/adafruit_data_parser.rb data/raw/screen-logs_flashdrive/des_Chiroptera-iMac13,2-OSX10,8,5`).

Output files will be available in the output/ directory. From these files (except csv) graphs can be make via plot/make_plot_script.sh.

## Further Instructions

To process the serial output, place your data files in a new subdir of data/raw. If you run Cam's ruby script from the parent directory they run correctly, e.g.,

ruby src/adafruit_data_parser.rb data/raw/sahil/atlab_at-l-c-211p-k02_Win7_6.1-7601-SP1 

To create pretty graphs, create subfolders in the output directory that aggregate a certain plot data across multiple machines, e.g., create a output/amperage_time directory and then move all atlab_*_amperage_time output files into it.

Then you can run

bash plot/make_plot_script.sh output/amperage_time title x y

And it will make a reasonable looking graph in the graphs directory. You will probably have to do some tweaking of the gnuplot/bash script in order to get it looking the way you’d like it to.
