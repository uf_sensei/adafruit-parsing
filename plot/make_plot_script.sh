#!/bin/bash 

if [ "$#" -ne 4 ]; then
    echo "Usage: $0 input_dir title xlabel ylabel"
    exit
fi

scriptfile="plot_script.p" 
input_dir="$1"
filename=${input_dir##*/}
title="$2"
xlabel="$3"
ylabel="$4"

# # delete temporary file 
rm $scriptfile

echo 'set terminal svg linewidth 3' >> $scriptfile 
echo 'output_dir = "graphs/"' >> $scriptfile
echo "set output output_dir . \"$filename-graph.svg\"" >> $scriptfile 

echo "set title \"$title\"" >> $scriptfile
echo "set xlabel \"$xlabel\"" >> $scriptfile 
echo "set ylabel \"$ylabel\"" >> $scriptfile 

# For CDF Plots
#echo "set yrange [0:1]" >> $scriptfile

# echo "set key out vert" >> $scriptfile
# echo "set key box" >> $scriptfile
# echo "set key right" >> $scriptfile

# one plot command only 
echo -n 'plot' >> $scriptfile 

counter=1
# iterate over *.dat files 
for i in $input_dir/* 
do 
   echo $input_dir
   echo -n "'$i' u 1:2 with linespoints notitle" >> $scriptfile 
   echo ', \' >> $scriptfile
   let "counter += 1"
done 

# the first plot is undefined. thus we start the real 
# plots with the second plot and can use a leading "," 
# all the time. 
echo -n ' 0/0 notitle' >> $scriptfile 


# #call gnuplot 
gnuplot $scriptfile 


